<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" href="http://energo.uz/wp-content/themes/newenergo/img/favicon.ico" type="image/x-icon"/>
    <link rel="stylesheet" href="http://energo.uz/wp-content/themes/newenergo/libs/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css"
          integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
    <link rel="stylesheet" href="http://energo.uz/wp-content/themes/newenergo/libs/slick/slick.css">
    <link rel="stylesheet" href="http://energo.uz/wp-content/themes/newenergo/libs/slick/slick-theme.css">
    <link rel="stylesheet" href="http://energo.uz/wp-content/themes/newenergo/libs/swiper/swiper.min.css">
    <link rel="stylesheet" href="http://energo.uz/wp-content/themes/newenergo/css/fonts.css">
    <link rel="stylesheet" href="http://energo.uz/wp-content/themes/newenergo/css/media.css">
    <link rel="stylesheet" href="http://energo.uz/wp-content/themes/newenergo/style.css">

    <link rel="stylesheet" type="text/css" href="http://energo.uz/wp-content/themes/newenergo/css/normalize.css"/>
    <link rel="stylesheet" type="text/css" href="http://energo.uz/wp-content/themes/newenergo/css/component.css"/>
    <script src="http://energo.uz/wp-content/themes/newenergo/js/modernizr.custom.js"></script>
    <title><?php wp_title(); ?></title>
</head>
<body>
<div class="container humburg">
    <ul id="gn-menu" class="gn-menu-main">
        <li class="gn-trigger">
            <a class="menu-bars">
                <img src="http://energo.uz/wp-content/themes/newenergo/img/HamburgerIcon.svg" alt="" height="25px"
                     width="30px" id="open-menu">
            </a>
            <nav class="gn-menu-wrapper">
                <div class="gn-scroller">

                    <div class="nav-head"> Nav head
                        <div class="n-logo"><img src="http://energo.uz/wp-content/themes/newenergo/img/square.png"
                                                 alt="Energo.uz"></div>
                        <div class="n-cont"><a href="info@energo.uz">email- info@energo.uz</a></div>
                        <div class="bg-nav"></div>
                    </div>
                    <ul class="gn-menu">

                        <li><a href="http://energo.uz/">Главная</a></li>
                        <li><a href="http://energo.uz/about">О компании</a></li>
                        <li><a href="http://energo.uz/product">Продукция</a></li>
                        <li><a href="http://energo.uz/product">Услуги</a></li>
                        <li><a href="http://energo.uz/list">Файлы</a></li>
                        <li><a href="http://energo.uz/contacts">Контакты</a></li>

                    </ul>
                </div>
                /gn-scroller
            </nav>
        </li>
        <li><a><?php the_title(); ?></a></li>
        <!-- <div class="search-div">
            <div class="s-form">
                <button><img src="http://energo.uz/wp-content/themes/newenergo/img/left.svg" alt="" width="25px"></button>
                <input type="text" placeholder="Поиск...">
                <img src="http://energo.uz/wp-content/themes/newenergo/img/close.svg" alt="" width="22px" class="close-s">
            </div>
        </div> -->
        <li class="nav-search">
            <!-- <img src="http://energo.uz/wp-content/themes/newenergo/img/search.svg" alt="" width="25px"> -->
        </li>
    </ul>
    <div class="full-hover"></div>
</div>
<!-- /container -->

<header> <!-- Header -->
    <div class="container extend">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="#">
                <img src="http://energo.uz/wp-content/themes/newenergo/img/logo.png" alt="">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                    aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item active">
                        <a class="nav-link" href="http://energo.uz">Техэнергореммаш<span
                                    class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" href="http://energo.uz/about">О компании</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="http://energo.uz/product">Продукция</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="http://energo.uz/product">Услуги</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="http://energo.uz/list">Файлы</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="http://energo.uz/contacts">Контакты</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link"> +998 (95) 195-42-61</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">email- info@energo.uz
                        </a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="false">
                            RU
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="#">UZ</a>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
</header>
