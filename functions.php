<?php function kama_recent_comments($limit = 10, $ex = 45, $cat = '', $echo = true)
{
    global $wpdb;
    if ($cat) {
        $IN = (strpos($cat, '-') === false) ? "IN ($cat)" : "NOT IN (" . str_replace('-', '', $cat) . ")";
        $JOIN = "LEFT JOIN $wpdb->term_relationships rel ON (p.ID = rel.object_id) 
        LEFT JOIN $wpdb->term_taxonomy tax ON (rel.term_taxonomy_id = tax.term_taxonomy_id)";
        $AND = "AND tax.taxonomy = 'category' 
        AND tax.term_id $IN";
    }
    $sql = "SELECT comment_ID, comment_post_ID, comment_content, post_title, guid, comment_author 
    FROM $wpdb->comments com 
        LEFT JOIN $wpdb->posts p ON (com.comment_post_ID = p.ID) $JOIN 
    WHERE comment_approved = '1' 
        AND comment_type = '' $AND 
    ORDER BY comment_ID DESC 
    LIMIT $limit";

    $results = $wpdb->get_results($sql);

    $out = '';
    foreach ($results as $comment) {
        $comtext = strip_tags($comment->comment_content);
        $leight = (int)iconv_strlen($comtext, 'utf-8');
        if ($leight > $ex) $comtext = iconv_substr($comtext, 0, $ex, 'UTF-8') . ' …';
        $out .= "\n<span class=funuser>" . strip_tags($comment->comment_author) . ": </span>" . "<a href='" . get_permalink($comment->comment_post_ID) . "#comment-{$comment->comment_ID}' title='к записи: {$comment->post_title}'>{$comtext}</a><br/>";
    }

    if ($echo) echo $out;
    else return $out;
} ?>