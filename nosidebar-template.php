<?php
/*   
Template Name: No Sidebar Layout
*/
?>

<?php get_header(); ?>

<?php get_sidebar(); ?>


    <section class="about">
        <div class="container extend">
            <div class="row">

                <div class="col-md-3 col-sm-12">

                </div>

                <div class="col-md-9">
                    <div class="content">
                        <?php if (have_posts()) : ?>
                            <?php while (have_posts()) : the_post(); ?>
                                <h4><?php the_title(); ?> </h4>
                                <img src="http://energo.uz/wp-content/themes/newenergo/img/image(1).png" alt="">

                                <?php the_content(''); ?>
                            <?php endwhile; ?>
                        <?php endif; ?>


                    </div>
                    <div class="clearfix"></div>
                </div>


            </div>
        </div>
    </section>

<?php get_footer(); ?>