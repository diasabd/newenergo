<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="ru">
<head>
    <link rel="icon" href="favicon.ico" type="image/x-icon"/>
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon"/>
    <meta name="robots" content="all"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="description" content="Описание"/>
    <meta name="keywords" content="ключи"/>
    <title><?php bloginfo('name'); ?><?php wp_title(); ?></title>
    <link href="http://energo.uz/wp-content/themes/energo/d/456057/t/v0/images/styles.css" rel="stylesheet"
          type="text/css"/>
    <link href="http://energo.uz/wp-content/themes/energo/d/456057/t/v0/images/styles_articles_tpl.css" rel="stylesheet"
          type="text/css"/>
    <link href="http://energo.uz/wp-content/themes/energo/d/456057/t/v0/images/slider/rhinoslider-1.05.css"
          rel="stylesheet" type="text/css"/>
    <?php wp_head(); ?>
</head>
<body>
<div class="table-div">
    <table width="100%" class="top-table">
        <tr>
            <td class="left-t"></td>
            <td class="center-t">
                <div id="site-wrap">
                    <div id="site-header">
                        <div class="site-logo"><a href="http://energo.uz"><img
                                        src="http://energo.uz/wp-content/themes/energo/d/456057/t/v0/images/logo.png"
                                        title="На Главную" alt="" border="0"/></a></div>
                        <div class="site-logotype"></div>
                        <div class="site-slogan">Производство и реализация электротехнического оборудования и
                            комплектующих
                        </div>
                        <div class="site-flash">

                        </div>
                        <div class="fax-top"><p><img
                                        src="http://energo.uz/wp-content/themes/energo/d/456057/t/images/phone.png"
                                        align="left"/>&nbsp; <strong>+998 (95) 195-42-61</strong></p></div>
                        <div class="mail-top"><p><img
                                        src="http://energo.uz/wp-content/themes/energo/d/456057/t/images/mail.png"
                                        align="left"/>&nbsp; <a href="mailto:info@energo.uz" title="info@energo.uz">info@energo.uz</a>
                            </p></div>
                        <div class="phone-top"><p><span style="font-size: 14px;"></span> <img
                                        src="http://energo.uz/wp-content/themes/energo/d/456057/t/images/fax.png"
                                        align="left"/>&nbsp; <strong>+998 (70) 983 40 54</strong></p></div>
                        <div class="site-top-box">
                            <ul class="menu-top">
                                <li><a href="http://energo.uz">Главная</a></li>
                                <li><a href="http://energo.uz/product">Продукция</a></li>
                                <li><a href="http://energo.uz/about">О компании</a></li>
                                <li><a href="http://energo.uz/contacts">Контакты</a></li>
                                <li><a href="http://energo.uz/list">Опросной лист</a></li>
                            </ul>
                        </div>
                        <div class="site-slider-box">

                            <ul id="site_slider">
                                <li><?php if (function_exists("easingslider")) {
                                        easingslider(19);
                                    } ?></li>
                            </ul>
                        </div>
                    </div>


                </div>
                <table class="table-default site-content">
                    <tr>
                        <td class="cell-dafault site-content-left">
                            <div class="site-link"><a href="http://energo.uz/geo"><img
                                            src="http://energo.uz/wp-content/themes/energo/d/456057/t/v0/images/geo_link.png"
                                            alt="" border="0"/></a></div>


                            <div class="menu-left-title">Каталог оборудования</div>

                            <ul id="menu-left">
                                <li><a href="http://energo.uz/shkafyi-0-4-kv"><span>Шкафы 0,4 КВ</span></a></li>
                                <li><a href="http://energo.uz/shkafyi-10kv"><span>Шкафы 10 КВ</span></a></li>
                                <li><a href="http://energo.uz/shity-rittal"><span>Щиты RITTAL</span></a></li>
                                <li><a href="http://energo.uz/shkafy-rittal"><span>Шкафы RITTAL</span></a></li>
                                <li><a href="http://energo.uz/podstantsii"><span>Подстанции</span></a></li>
                                <li><a href="http://energo.uz/metallokorpusa"><span>Металлокорпуса</span></a></li>
                                <li><a href="http://energo.uz/razediniteli"><span>Разъединители</span></a></li>
                                <li><a href="http://energo.uz/yashhiki"><span>Ящики управления</span></a></li>
                                <li><a href="http://energo.uz/komplektuyushhie"><span>Комплектующие</span></a></li>


                            </ul>
                        </td>
                        <td class="cell-dafault site-content-middle">
                            <?php if (have_posts()) : ?>
                            <?php while (have_posts()) :
                            the_post(); ?>
                            <h1><?php the_title(); ?>        </h1>
                            <p style="text-align: justify;">
    <?php the_content(''); ?></div>
<?php endwhile; ?>
<?php endif; ?>
</p>
</div>


</td>

</tr>
</table>
<div id="site-footer">
    <div id="site-footer-l">
        <div id="site-footer-r">
            <ul class="menu-bottom">
                <li><a href="http://energo.uz">Главная</a></li>
                <li><a href="http://energo.uz/product">Продукция</a></li>
                <li><a href="http://energo.uz/about">О компании</a></li>
                <li><a href="http://energo.uz/contacts">Контакты</a></li>
                <li><a href="http://energo.uz/list">Опросной лист</a></li>

            </ul>
            <div class="site-name-bottom"> &copy; 2015</div>
            <div class="site-copyright"></div>
            <div class="address-bottom">Наш адрес: 111900, Ташкентская область, Юкори-Чирчикский район, поселок
                Янгибазар, улица Карасу 100
            </div>
            <div class="phone-bottom"><span class="pbs">Телефон :</span>
                <p> +998 (95) 195-42-61</p> E-mail: <a href="mailto:info@energo.uz" title="info@energo.uz">info@energo.uz</a>
            </div>
            <div class="site-counters"><!--LiveInternet counter-->
                <script type="text/javascript"><!--
                    document.write("<a href='//www.liveinternet.ru/click' " +
                        "target=_blank><img src='//counter.yadro.ru/hit?t16.6;r" +
                        escape(document.referrer) + ((typeof(screen) == "undefined") ? "" :
                            ";s" + screen.width + "*" + screen.height + "*" + (screen.colorDepth ?
                            screen.colorDepth : screen.pixelDepth)) + ";u" + escape(document.URL) +
                        ";" + Math.random() +
                        "' alt='' title='LiveInternet: показано число просмотров за 24" +
                        " часа, посетителей за 24 часа и за сегодня' " +
                        "border='0' width='88' height='31'><\/a>")
                    //--></script><!--/LiveInternet-->

            </div>
        </div>
    </div>
</div>

<div class="center-div"><img src="http://energo.uz/wp-content/themes/energo/d/456057/t/images/spacer.gif" width="980"
                             height="0" alt=""/></div>
</td>
<td class="right-t"></td>
</tr>
</table>
</div>
</body>
</html>