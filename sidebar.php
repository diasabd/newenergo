<section class="slide-content">
    <div class="container extend-fluid">
        <div class="swiper-container">
            <!-- Additional required wrapper -->
            <div class="swiper-wrapper">
                <!-- Slides -->
                <div class="swiper-slide">
                    <img src="http://energo.uz/wp-content/themes/newenergo/img/slider-partner.png" alt="">
                    <div class="swiper-content">
                        <h2>
                            Стабильный и надежный партнер
                            <br>

                        </h2>
                        <span>Более 12 лет на рынке Средней Азии</span> </br>
                        <a class="btn">Подробнее</a>
                    </div>
                </div>
                <div class="swiper-slide">
                    <img src="http://energo.uz/wp-content/themes/newenergo/img/slider-construction.jpg" alt="">
                    <div class="swiper-content">
                        <h2>Конструирование, монтаж и <br>
                            наладка энергообъектов под ключ
                        </h2>
                        <span>Индивидуальный подход</span> </br>
                        <a class="btn">Подробнее</a>
                    </div>
                </div>
                <div class="swiper-slide">
                    <img src="http://energo.uz/wp-content/themes/newenergo/img/slider-maintaince.jpg" alt="">
                    <div class="swiper-content">
                        <h2>Ремонт силовых и тяговых трансформаторов <br>

                        </h2>
                        <span>Текущий, средний, капитальный</span> </br>
                        <a class="btn">Подробнее</a>
                    </div>
                </div>
                <div class="swiper-slide">
                    <img src="http://energo.uz/wp-content/themes/newenergo/img/slider-lab.jpg" alt="">
                    <div class="swiper-content">
                        <h2>
                            Мобильные лаборатории для диагностики и <br>
                            испытаний трансформаторов
                        </h2>
                        <span>Проведение широкого спектра анализов на месте установки оборудования</span> </br>
                        <a href="http://energo.uz/mobilnye-ispytatelnye-laboratorii" class="btn">Подробнее</a>
                    </div>
                </div>
                ...
            </div>
            <!-- If we need pagination -->
            <div class="swiper-pagination"></div>
            <!-- If we need navigation buttons -->
            <div class="swiper-button-prev"></div>
            <div class="swiper-button-next"></div>
        </div>
    </div>
</section>