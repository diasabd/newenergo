<?php
/*   
Template Name: Contact Layout
*/
?>

<?php get_header(); ?>

    <section class="slide-content">
        <div class="container extend-fluid">
            <div class="swiper-container">
                <!-- Additional required wrapper -->
                <div class="swiper-wrapper">
                    <!-- Slides -->
                    <div class="swiper-slide">
                        <img src="http://energo.uz/wp-content/themes/newenergo/img/image.png" alt="">
                        <div class="swiper-content">
                            <h2>
                                Мы предоставляем лучшие <br>
                                услуги для энергетики
                            </h2>
                            <span>Более 30 профессионалов</span> </br>
                            <a class="btn">Подробнее</a>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <img src="http://energo.uz/wp-content/themes/newenergo/img/image.png" alt="">
                        <div class="swiper-content">
                            <h2>
                                Мы предоставляем лучшие <br>
                                услуги для энергетики
                            </h2>
                            <span>Более 30 профессионалов</span> </br>
                            <a class="btn">Подробнее</a>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <img src="http://energo.uz/wp-content/themes/newenergo/img/image.png" alt="">
                        <div class="swiper-content">
                            <h2>
                                Мы предоставляем лучшие <br>
                                услуги для энергетики
                            </h2>
                            <span>Более 30 профессионалов</span> </br>
                            <a class="btn">Подробнее</a>
                        </div>
                    </div>
                    ...
                </div>
                <!-- If we need pagination -->
                <div class="swiper-pagination"></div>

                <!-- If we need navigation buttons -->
                <div class="swiper-button-prev"></div>
                <div class="swiper-button-next"></div>
            </div>
        </div>
    </section>

    <section class="form-content extend">
        <div class="container">
            <h2>Контакты</h2>

            <form>
                <div class="form-row">
                    <div class="col-md-4">
                        <input type="text" class="form-control" placeholder="Ваше Имя">
                    </div>
                    <div class="col-md-4">
                        <input type="text" class="form-control" placeholder="Ваш Телефон">
                    </div>
                    <div class="col-md-4">
                        <input type="text" class="form-control" placeholder="Ваш e-mail">
                    </div>
                    <div class="col-md-8">
                        <textarea name="" id="" placeholder="Ваше Сообщение"></textarea>
                    </div>
                    <div class="col-md-4 reset-p">
                        <img src="http://energo.uz/wp-content/themes/newenergo/img/image(2).png" alt="">
                        <span>
                            <h4>Служба поддержки.</h4>
                            <p>По всем вопросам оброщайтесь <br> по номеру:</p>
                            <p>Моб.: +998 (95) 195-42-61</p>
                            <p>Телефон: +998 (70) 983 40 54</p>
                        </span>
                    </div>
                </div>


            </form>
        </div>
    </section>

    <section class="map">
        <div class=" extend-fluid">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2997.293558303678!2d69.52142631483618!3d41.30247700921859!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x38ae57e4f2344f2f%3A0xc856b3d3fa324a5e!2z0KHQnyDQotCV0KXQrdCd0JXQoNCT0J7QoNCV0JzQnNCQ0Kg!5e0!3m2!1sru!2s!4v1531478587883"
                    width="100%" height="576" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
    </section>

<?php get_footer(); ?>