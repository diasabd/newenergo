<footer>
    <div class="container ">
        <div class="row">

            <div class="col-md-4 col-sm-6 energo">
                <div>
                    <img src="http://energo.uz/wp-content/themes/newenergo/img/square.png" alt="">
                </div>
                <h4 class="bold">Техэнергореммаш</h4>
                <p>Ремонт, монтаж, модернизация и испытания трансформаторных подстанции</p>
            </div>

            <div class="col-md-4 col-sm-6">
                <h4 class="bold">Меню</h4>
                <ul class="ft50">
                    <li><a href="">Продукция</a></li>
                    <li><a href="">О компании</a></li>
                    <li><a href="">Контакты</a></li>
                </ul>
                <ul class="ft50">
                    <li><a href="">Название</a></li>
                    <li><a href="">Категория</a></li>
                </ul>
            </div>

            <div class="col-md-4 col-sm-12">
                <h4 class="bold">Контакты</h4>
                <ul class="cont">
                    <li>
                        <i class="fas fa-map-marker-alt"></i>
                        <span>111900, Ташкентская область, Юкори-Чирчикский район, поселок Янгибазар, улица Карасу 100</span>
                    </li>
                    <li>
                        <i class="fas fa-phone"></i>
                        <span>+998 (70) 983 40 54 ,  +998 (95) 195-42-61</span>
                    </li>
                    <li>
                        <i class="fas fa-briefcase"></i>
                        <span> Юридическая информация. Ограничение ответсвенности </span>
                    </li>
                </ul>
            </div>

        </div>
    </div>
</footer>
<div class="credits">
    <div class="container">
        © 2018 Техэнергореммаш.
    </div>
</div>


<script src="http://energo.uz/wp-content/themes/newenergo/libs/swiper/swiper.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
<script src="http://energo.uz/wp-content/themes/newenergo/js/script.js"></script>
<script src="http://energo.uz/wp-content/themes/newenergo/libs/slick/slick.min.js"></script>
<script src="http://energo.uz/wp-content/themes/newenergo/js/classie.js"></script>
<script src="http://energo.uz/wp-content/themes/newenergo/js/gnmenu.js"></script>
<script>
    new gnMenu(document.getElementById('gn-menu'));
</script>
</body>
</html>