var mySwiper = new Swiper ('.swiper-container', {
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
    scrollbar: {
      el: '.swiper-scrollbar',
    },
  })

  $(document).ready(function(){
    $('.production-slider').slick({
      dots: true,
      infinite: false,
      speed: 300,
      slidesToShow: 4,
      slidesToScroll: 4,
      responsive: [
        {
          breakpoint: 1170,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            infinite: true,
            dots: true
          }
        },
        {
          breakpoint: 991,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ]
    });
  })


$('.nav-search').click(function(){
  $('.nav-search').hide();
  $('.search-div').css('display', 'block')


});

$('.close-s').click(function(){
  $('.nav-search').show();
  $('.search-div').css('display', 'none')

});


 $(".open-ul>a").click(function(){
        $('.open-ul ul').slideToggle('100');
    });

