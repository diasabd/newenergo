<?php get_header(); ?>
<?php get_sidebar(); ?>

    <section class="about">
        <div class="container extend">
            <div class="row">

                <div class="col-md-3 col-sm-12">
                    <div class="left-bar">
                        <ul class="list-unstyled">

                            <li class="under-nav"><a href="">Оборудование 0,4 кВ</a>
                                <ul>
                                    <li><a href="http://energo.uz/shkafyi-0-4-kv"><span>Шкафы 0,4 КВ</span></a></li>
                                    <li><a href="http://energo.uz/podstantsii"><span>Подстанции</span></a></li>
                                    <li><a href="http://energo.uz/yashhiki"><span>Ящики управления</span></a></li>
                                    <li><a href="http://energo.uz/komplektuyushhie"><span>Комплектующие</span></a></li>
                                </ul>
                            </li>
                            <li class="under-nav"><a href="">Оборудование 10 кВ</a>
                                <ul>
                                    <li><a href="http://energo.uz/podstantsii"><span>Подстанции</span></a></li>
                                    <li><a href="http://energo.uz/shkafyi-10kv"><span>Шкафы 10 КВ</span></a></li>
                                    <li><a href="http://energo.uz/metallokorpusa"><span>Металлокорпуса</span></a></li>
                                    <li><a href="http://energo.uz/razediniteli"><span>Разъединители</span></a></li>
                                    <li><a href="http://energo.uz/yashhiki"><span>Ящики управления</span></a></li>
                                </ul>
                            </li>

                            <li class="under-nav"><a href="http://energo.uz/komplektuyushhie">Комплектующие</a>
                                <ul>
                                    <li><a href="http://energo.uz/komplektuyushhie/avtomaticheskie-vyiklyuchateli"><span>Автоматические выключатели</span></a></li>
                                    <li><a href="http://energo.uz/komplektuyushhie/kontaktoryi-i-rele"><span>Контакторы и реле</span></a></li>
                                    <li><a href="http://energo.uz/komplektuyushhie/modulnyie-avtomaticheskie-vyiklyuchateli"><span>Модульные автоматические выключатели</span></a></li>
                                </ul>
                            </li>


                        </ul>
                    </div>
                </div>

                <div class="col-md-9">
                    <div class="content">
                        <?php if (have_posts()) : ?>
                            <?php while (have_posts()) : the_post(); ?>
                                <h4><?php the_title(); ?> </h4>

                                <?php the_content(''); ?>
                            <?php endwhile; ?>
                        <?php endif; ?>


                    </div>
                    <div class="clearfix"></div>
                </div>


            </div>
        </div>
    </section>

<?php get_footer(); ?>